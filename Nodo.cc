//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

//#include "Nodo.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <omnetpp.h>
#include <ctopology.h>
#include <time.h>
#include <list>
#include <algorithm>
#include <iostream>
#include <fstream>

#include "Ping_m.h"

using namespace std;

#define GOSSIP_PE 0
#define GOSSIP_PB 1
#define GOSSIP_FF 2

#define END_CLOCK 500
#define MAX_CACHE_SIZE 512
#define MAX_MSGS 0
#define MAXINT 2147483646

ofstream tracefile;
long unsigned int ID_COUNT;
unsigned int MSG_COUNT;
double PROBAB_TRANS;
unsigned int max_ttl;
simtime_t MEAN;

class Nodo: public cSimpleModule {
    int type; 
    unsigned int id;
    unsigned int *neighbors_ids;

    unsigned int gate_size;
    simtime_t next_msg_gen;
    list<unsigned long> cache;
    unsigned int cache_size;
    protected:
    // The following redefined virtual function holds the algorithm.
    virtual void initialize();
    virtual void finish();
    virtual void delayedCreation();
    virtual double getUniformRandom(double start, double end);
    virtual void handleMessage(cMessage *msg);
    virtual void broadcast(Ping* msg);
    virtual void sendM(Ping *msg, unsigned int oldSenderId);
};

Define_Module(Nodo);

void Nodo::initialize() {
    char* trace_dirname;
    char* trace_filename;
    char* type_string;
    cGate *rcvgate;
    Nodo* rcvnode;

    /*get node id from assigned name*/
    sscanf(getName(),"n%u", &id);
    
    /*get algorithm, trace directory,ttl and algorithm specific probability from env variables*/
    type_string=getenv("GOSSIP_ALGO");
    if(type_string==NULL) {
        perror("getenv GOSSIP_ALGO failed:");
        exit(-1);
    }
    type=atoi(type_string);
    sscanf(getName(),"n%u", &id);
    trace_filename=(char*) malloc(1024);
    if(trace_filename==NULL) {
        perror("error allocating memory:");
        exit(-1);
    }
    trace_dirname=getenv("TRACE_DIR");
    if(trace_dirname==NULL) {
        perror("getenv TRACE_DIR failed:");
        exit(-1);
    }
    max_ttl=atoi(getenv("MAX_TTL"));
    if(max_ttl==NULL) {
        perror("getenv MAX_TTL failed:");
        exit(-1);
    }
    strcpy(trace_filename, trace_dirname);
    strcat(trace_filename, "/SIM_TRACE_000.log");
    PROBAB_TRANS=atof(getenv("REAL_PROB"));
    if(id==0){
        tracefile.open (trace_filename);
        ID_COUNT=1;
    }

    /*save neighbors ids to improve performance*/
    gate_size=gateSize("g");
    neighbors_ids = new unsigned int[gate_size];
    for(unsigned int i=0; i<gate_size; i++) {
        rcvgate=gate("g$o", i)->getNextGate();
        rcvnode=check_and_cast<Nodo *>(rcvgate->getOwnerModule());
        sscanf(rcvnode->getName(),"n%u", &neighbors_ids[i]);
    }
    
    /*the cache is empty*/
    cache_size=0;

    free(trace_filename);
    next_msg_gen=SIMTIME_ZERO;
    MEAN=1;
    /*the first message creation (delayed)*/
    delayedCreation();
}

/*finish the simulation*/
void Nodo::finish() {
    if(id == 0) {
        tracefile.close();
    }
    delete [] neighbors_ids;
}

/*generate a message at the time chosen with an exponential probability*/
void Nodo::delayedCreation() {
    next_msg_gen+=(exponential(MEAN)*10);
    /*we have support for setting a max number of messages*/
    if (MAX_MSGS==0 || MSG_COUNT < MAX_MSGS) {
        MSG_COUNT++;
        /*if the message can finish its propagation, generate it and send it at the chosen time*/
        if (SIMTIME_DBL(next_msg_gen)+max_ttl <= END_CLOCK) {
            cMessage *msg=new cMessage("delay");
            scheduleAt(next_msg_gen, msg);
        }
    }
}
/*get a random double uniformly distributed in [start,end]*/
double Nodo::getUniformRandom(double start, double end) {
    int intrand;
    double randval;
    intrand = intuniform(0, MAXINT);
    randval = (double) intrand/(double) MAXINT;
    randval= (double) start + randval*((double ) (end - start));
    return randval;
}

/*handle any message received
 *
 * msg: the message received, can be a ping message or a
 * message create synchronization message */
void Nodo::handleMessage(cMessage *msg) {
    Ping *rcv_ping; 
    Ping *new_ping;
    unsigned int ttl;
    long unsigned int msgId;
    unsigned int sender;
    
    /*we must create a new message to send it*/
    new_ping=new Ping("p");
    new_ping->setSender(this->id);

    /*if we received a ping message, save the relevant info*/
    if (dynamic_cast<Ping *>(msg)) {
        rcv_ping = check_and_cast<Ping *>(msg);
        sender = rcv_ping->getSender();
        new_ping->setOrigin(rcv_ping->getOrigin());
        /*decrement ttl*/
        ttl=rcv_ping->getTtl()-1;
        new_ping->setTtl(ttl);
        msgId=rcv_ping->getMsgId();
        new_ping->setMsgId(msgId);
        /*write to trace file a receive event*/
        tracefile << "R "<<id<<" "<<msgId<<" "<<max_ttl-ttl<<" "<<ttl<<"\n";
        delete(rcv_ping);
    }
    /*if we receive a synchronization message, set relevant info*/
    else {
        sender = this->id;
        new_ping->setOrigin(this->id);
        ttl=max_ttl;
        new_ping->setTtl(ttl);
        ID_COUNT++;
        new_ping->setMsgId(ID_COUNT);
        msgId=new_ping->getMsgId();
        delete(msg);
        /*write a generation event and a "first receive" event on the trace*/ 
        tracefile << "G "<<new_ping->getMsgId()<<"\n";
        tracefile << "R "<<id<<" "<<new_ping->getMsgId()<<" 0\n";
        /*start propagation*/
        broadcast(new_ping);
        delayedCreation();
        delete(new_ping);
        /*return, we have completed the first dissemination step*/
        return;
    }
    /*if the ttl hasn't expired*/
    if((ttl>0)){
        /*if the message is not in the local cache*/
        if(find(cache.begin(), cache.end(), msgId)==cache.end() || cache_size==0) {
            /*if the cache is enabled (size>0)*/
            if(MAX_CACHE_SIZE>0) {
                /*if the cache is full, pop from it, else increment its size*/
                if(cache_size==MAX_CACHE_SIZE) {
                    cache.pop_back();
                }
                else {
                    cache_size++;
                }
                /*insert the new message in the cache anyway*/
                cache.push_front(msgId);
            }
            /*send the message with the appropriate algorithm*/
            sendM(new_ping, sender);
        }
        /*if message is in the cache, put it on top and stop the propagation*/
        else {
            cache.remove(msgId);
            cache.push_front(msgId);
            delete(new_ping);
        }
    }else{
        delete(new_ping);
    }
}

/*send a ping message to all neighbors*/
void Nodo::broadcast(Ping* msg) {
    cGate *gout;
    int i;
    for(i=0; i<gate_size; i++) {
        gout = gate("g$o", i);
        send(msg->dup(), gout);
    }
}

/* send a message with the selected algorithm
 *
 * msg:  the message to send
 * oldSenderId: the sender of the message*/
void Nodo::sendM(Ping *msg, unsigned int oldSenderId) {
    cGate *gout;
    unsigned int i;
    double rand;
    switch (type) {
        case GOSSIP_PE: // gossip probabilistic edge
            for(i=0; i<gate_size; i++) {
                rand=getUniformRandom(0,100);
                if(rand<=PROBAB_TRANS) {
                    gout = gate("g$o", i);
                    /*check if the selected neighbor is the sender or the creator*/
                    if (neighbors_ids[i] != oldSenderId && neighbors_ids[i] != msg->getOrigin())
                        send(msg->dup() ,gout);
                }
            }
            break;
        case GOSSIP_PB: // gossip probabilistic broadcast
            rand=getUniformRandom(0,100);
            if(rand<=PROBAB_TRANS) {
                for(i=0; i<gate_size; i++) {
                    gout = gate("g$o", i);
                    /*check if the selected neighbor is the sender or the creator*/
                    if ( neighbors_ids[i] != oldSenderId && neighbors_ids[i] != msg->getOrigin())
                        send(msg->dup(), gout);
                }
            }
            break;
        case GOSSIP_FF: // gossip fixed fanout
            int r, new_gsize;
            std::list<int> temp;
            std::list<int>::iterator it;
            new_gsize=0;
            /*save neighbors in a temporary list*/
            for(i=0; i<gate_size; i++) {
                gout = gate("g$o", i);
                /*we exclude from the list the old sender and the message creator*/
                if ( neighbors_ids[i] != oldSenderId && neighbors_ids[i] != msg->getOrigin()) {
                    temp.push_back(i);
                    new_gsize++;
                }    
            }
            i=1;
            /*while the neighbors list is not empty and we did not send to fanout nodes*/
            while(i<=PROBAB_TRANS && !temp.empty()) {
                it = temp.begin();
                /*get a random node index and send it the messsage*/
                r = intuniform(0,new_gsize-i);
                std::advance(it, r);
                gout = gate("g$o", *it);
                send(msg->dup(), gout);
                temp.remove(*it);
                i++;
            }
            break;
    }
    delete(msg);
}
