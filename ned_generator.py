#!/usr/bin/env python

import lxml
import sys
import xml.etree.ElementTree as ET

#parse xml from graphml and get root element 
tree = ET.parse(sys.argv[1])
root = tree.getroot()
#the beginning of the ned file
standard_init = """
package randgraph;

simple Nodo {
    parameters:
        @display("i=abstract/router_s,blue");
    gates:
        inout g[];
}

channel Arco extends ned.DatarateChannel {
    parameters:
    	delay=1s;
}

network Network {
    submodules:
"""
#nodes and edges arrays
nodes = [i for i in root[0] if 'node' in i.tag]
edges = [i for i in root[0] if 'edge' in i.tag]

#open output ned file and write the beginning
f = open(sys.argv[2],'w')
f.write(standard_init)

#write nodes infos
for node in nodes:
	f.write('\t\t'+node.get('id')+': Nodo;\n')
f.write('\tconnections:\n')
#write edges infos
for edge in edges:
	f.write('\t\t' + edge.get('source') + '.g++ <--> Arco <--> ' + edge.get('target') + '.g++;\n')
f.write('}\n')

f.close()
