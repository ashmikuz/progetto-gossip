#!/bin/bash

NUMBERRUNS=10;
NUMBERNODES=500;
CORPUS_DIRECTORY="$HOME/tmp/corpus";
TRACES_DIR="$HOME/tmp/traces";
RESULTS_DIR="$HOME/tmp/results";

GOSSIP_PE=0
GOSSIP_PB=1
GOSSIP_FF=2

#WARNING set algorithm here!!
export GOSSIP_ALGO=1;

function ceil () {
echo "define ceil (x) {if (x<0) {return x/1} \
else {if (scale(x)==0) {return x} \
else {return x/1 + 1 }}} ; ceil($1)" | bc;
}

MAX_EFFECTUAL_FANOUT=1;
MAX_FANOUT=1;

#get graphs infos (degree, max degree) and calculate Max effectual fanout and max degree,
#convert graphml to nedfile
for((RUN=1;RUN<=NUMBERRUNS; RUN++)) do
    DEGREE[$RUN]=$(./get_mean_degree "$CORPUS_DIRECTORY/test-graph-$RUN.graphml")
    MAX_DEGREE[$RUN]=$(./get_max_degree "$CORPUS_DIRECTORY/test-graph-$RUN.graphml")

    echo "Degree for $RUN is ${DEGREE[$RUN]}"
    if [ $(echo "$(ceil ${DEGREE[$RUN]}) >= $MAX_EFFECTUAL_FANOUT" | bc -l) -eq 1 ]; then
        MAX_EFFECTUAL_FANOUT=$(ceil ${DEGREE[$RUN]})
    fi
    if [ ${MAX_DEGREE[$RUN]} -ge $MAX_FANOUT ]; then
        MAX_FANOUT=${MAX_DEGREE[$RUN]}
    fi
    mkdir -p ./$RUN;
    #convert graphml to nedfile and copy it to the traces dir
    ./ned_generator.py "$CORPUS_DIRECTORY/test-graph-$RUN.graphml" "./$RUN/graph.ned";
    cp ./package.ned ./$RUN
done

#Fixed Fanout algorithm
if [ $GOSSIP_ALGO -eq $GOSSIP_FF ]; then
    echo "Max fanout is $MAX_FANOUT"
    #iterate fanout
    for((FANOUT=1; FANOUT<=MAX_FANOUT;FANOUT++)) do
        #iterate runs
        for((RUN=1;RUN<=NUMBERRUNS; RUN++)) do
            export REAL_PROB=$FANOUT
            export TRACE_DIR=$TRACES_DIR/$RUN;
            
            #set ttl to graph diameter
            export MAX_TTL=$(./get_diameter "$CORPUS_DIRECTORY/test-graph-$RUN.graphml")
            
            echo "TTL is $MAX_TTL"
            mkdir -p $TRACE_DIR;
            echo "Probability is: $REAL_PROB"
            echo $RUN;
            mkdir -p "$RESULTS_DIR/fxf-$FANOUT/$RUN";
            
            #simulate and get time
            TIME_VAL=$(($(date +%s%3N)))
            ./progetto-gossip -n ./$RUN;
            echo $(($(($(date +%s%3N))) - $TIME_VAL)) > $RESULTS_DIR/fxf-$FANOUT/$RUN/time
            
            #get stats and remove traces
            ./get_stats $TRACE_DIR $NUMBERNODES 1 >> "$RESULTS_DIR/fxf-$FANOUT/$RUN/stats";
            rm "$TRACES_DIR/$RUN/SIM_TRACE_000.log";
            
            #get this graph max degree and average degree probability
            GRAPH_MAX_DEGREE=${MAX_DEGREE[$RUN]}
            DEG_PROB=$(./get_degree "$CORPUS_DIRECTORY/test-graph-$RUN.graphml")

            #save degree probability in an array
            for ((I=1; I<=GRAPH_MAX_DEGREE; I++)) do
                P[$I]=$( echo "$DEG_PROB" | awk -v line="$I" 'NR==line { printf $2 }' )
            done
            for ((I=GRAPH_MAX_DEGREE+1; I<=NUMBERNODES;I++)) do
                P[$I]=0
            done

            #calculate EFF
            EFFECTIVE_FANOUT=0;
            for ((I=1;I<FANOUT;I++)) do
                echo "$EFFECTIVE_FANOUT+${P[$I]}*$I";
                EFFECTIVE_FANOUT=$(echo "$EFFECTIVE_FANOUT+${P[$I]}*$I;" | bc -l)
            done
             
            for ((I=FANOUT;I<=GRAPH_MAX_DEGREE;I++)) do
                echo "$EFFECTIVE_FANOUT+(${P[$I]}*$FANOUT);"
                EFFECTIVE_FANOUT=$(echo "$EFFECTIVE_FANOUT+(${P[$I]}*$FANOUT);" | bc -l)
            done
            
            #write eff to the results
            echo $EFFECTIVE_FANOUT >> "$RESULTS_DIR/fxf-$FANOUT/$RUN/stats";
        done
    done
#Probabilistic Broadcast and Probabilistic Edge
else
    echo "Max fanout is $MAX_EFFECTUAL_FANOUT";
    #iterate EFF
    for((EFFECTUAL_FANOUT=1; EFFECTUAL_FANOUT<=MAX_EFFECTUAL_FANOUT;EFFECTUAL_FANOUT++)) do
        for((RUN=1;RUN<=NUMBERRUNS; RUN++)) do
            export TRACE_DIR=$TRACES_DIR/$RUN;
            
            #set TTL to graph diameter
            export MAX_TTL=$(./get_diameter "$CORPUS_DIRECTORY/test-graph-$RUN.graphml")
            echo "Il ttl è $MAX_TTL"
            mkdir -p $TRACE_DIR;
            
            #get "real" probability from EFF
            export REAL_PROB=$(echo "$EFFECTUAL_FANOUT*100/${DEGREE[$RUN]}" | bc -l)
            echo "Prob is: $REAL_PROB"
            echo $RUN;
            mkdir -p "$RESULTS_DIR/eff-$EFFECTUAL_FANOUT/$RUN";
            
            #execute and save time
            TIME_VAL=$(($(date +%s%3N)))
            ./progetto-gossip -n ./$RUN;
            echo $(($(($(date +%s%3N))) - $TIME_VAL)) > $RESULTS_DIR/eff-$EFFECTUAL_FANOUT/$RUN/time
            
            #get stats and remove traces
            ./get_stats $TRACE_DIR $NUMBERNODES 1 >> "$RESULTS_DIR/eff-$EFFECTUAL_FANOUT/$RUN/stats";
            rm "$TRACE_DIR/SIM_TRACE_000.log"
        done
    done
fi
