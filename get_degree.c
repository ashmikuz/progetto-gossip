#include <stdio.h>
#include <igraph.h>

int main(int argc, char **argv) {

    igraph_vector_t adjv;
    FILE* graphmlfile;
    igraph_t g;
    int maxdegree, size, vertexcount,i, edgecount, testint;
    int *degrees;
    double prob_degree, testdouble;

    if(argc<1) {
        printf("Usage: %s graphmlfile", argv[0]);
    }

    graphmlfile=fopen(argv[1], "r");
    if(graphmlfile==NULL) {
        perror("Error: ");
        return(-1);
    }
    if(igraph_read_graph_graphml(&g, graphmlfile, 0)) {
        fprintf(stderr,"Error parsing graph");
        return(-1);
    }

    fclose(graphmlfile);

    vertexcount=igraph_vcount(&g);
    
    degrees=calloc(sizeof(int), vertexcount);
    
    edgecount=igraph_ecount(&g);

    maxdegree=0;
    testint=0;
    testdouble=0;
    
    for(i=0;i<vertexcount;i++) {
        igraph_vector_init(&adjv, 0);
        igraph_neighbors(&g,&adjv , i, IGRAPH_ALL);
        size=igraph_vector_size(&adjv);
        degrees[size]++;
        if(size>maxdegree) maxdegree=size;
        igraph_vector_destroy(&adjv);
    }

    for(i=1;i<=maxdegree;i++){
        prob_degree= (double) degrees[i] / (double) vertexcount;
        testdouble += prob_degree;
        printf("%d\t%f\n", i, prob_degree);
        //printf("%d\t%f\n", i, testdouble);
    }
}
