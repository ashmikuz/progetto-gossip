#include <igraph.h>
#include <stdio.h>


int main (int argc, char** argv) {
    FILE* graphmlfile;
    igraph_t g;
    igraph_integer_t diameter;

    if(argc!=2) {
        printf("%s graphfile\n", argv[0]);
        exit(-1);
    }

    graphmlfile=fopen(argv[1],"r");
    if(graphmlfile==NULL){
        perror("Error opening graph:");
        exit(-1);
    }

    if(igraph_read_graph_graphml(&g, graphmlfile, 0)) {
        fprintf(stderr,"Error parsing graph");
        return(-1);
    }

    fclose(graphmlfile);
    igraph_diameter(&g, &diameter, 0, 0, 0, IGRAPH_UNDIRECTED, 1);
    
    printf("%d\n", diameter);


}
