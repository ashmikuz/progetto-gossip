#!/usr/bin/env python2

base_ttl = 30
inc_ttl = 1
max_ttl = 50
base_cache = 256
inc_cache = 256
max_cache = 256
max_msgs = 0
dir = '/home/duma/tmp'
gdir = dir + '/graphs/'
gossips = ['2']
download_graphs = True
#archive_name = 'grafi.tar.gz'
#graphs_url = 'https://dl.dropboxusercontent.com/u/35664686/' + archive_name
archive_name = 'graph-1000.tar.gz'
graphs_url = 'https://dl.dropboxusercontent.com/u/23040379/' + archive_name
change = 'cache'

import os

def run(command):
	r = os.system(command)
	if r != 0:
		exit(-1)

def sed(r, s, file):
	run('sed -i \'s/' + r.replace('/','\/') + '/' + s.replace('/','\/') + '/g\' '+ file)

run('mkdir -p '+dir)
run('rm -rf '+dir+'/res*')
run('mkdir -p '+gdir)
if download_graphs:
	run('rm -rf '+gdir+'*')
	run('wget '+graphs_url+' -P '+gdir)
	run('tar xf '+gdir + archive_name +' -C '+gdir)
	run('rm -f '+gdir + archive_name)
graphs = os.listdir(gdir)

# imposto valori di default
#sed('define MAX_TTL [0-9]+',
#	'define MAX_TTL '+str(base_ttl),
#	'Nodo.cc')
sed('define MAX_CACHE_SIZE [0-9]+',
	'define MAX_CACHE_SIZE '+str(base_cache),
	'Nodo.cc')
sed('export GOSSIP_ALGO=[0-9a-zA-Z$_]*',
	'export GOSSIP_ALGO='+ gossips[0],
	'executemulti.sh')
sed('CORPUS_DIRECTORY="[a-zA-Z/$]*"',
	'CORPUS_DIRECTORY="'+dir+'/corpus"',
	'executemulti.sh')
sed('TRACES_DIR="[a-zA-Z/$]*"',
	'TRACES_DIR="'+dir+'/traces"',
	'executemulti.sh')
sed('RESULTS_DIR="[a-zA-Z/$]*"',
	'RESULTS_DIR="'+dir+'/results"',
	'executemulti.sh')
sed('define MAX_MSGS [0-9]+',
	'define MAX_MSGS '+str(max_msgs),
	'Nodo.cc')

for graph in graphs:
	run('rm -rf '+dir+'/corpus')
	run('ln -s '+ gdir +graph+' '+dir+'/corpus')
	for gossip in gossips:
		sed('export GOSSIP_ALGO=[0-9]',
			'export GOSSIP_ALGO=' + gossip,
			'executemulti.sh')
		cache = base_cache
		while cache <= max_cache:
			print 'Ricompilo il programma con cache '+ str(cache)
			run('make clean')
			run('rm -rf '+dir+'/traces/*')
			sed('define MAX_CACHE_SIZE [0-9]+',
				'define MAX_CACHE_SIZE ' + str(base_cache),
				'Nodo.cc')
			run('make')
			run('./executemulti.sh')
			run('mv '+dir+'/results '+dir+'/res-'+graph+'-alg'+gossip+'-cache'+str(cache))
			cache = cache + inc_cache
