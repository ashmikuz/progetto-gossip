#include <stdio.h>
#include <igraph.h>

int main(int argc, char **argv) {

    igraph_vector_t adjv;
    FILE* graphmlfile;
    igraph_t g;
    int maxdegree, size, vertexcount,i, edgecount, testint;
    int *degrees;
    double prob_degree, testdouble;

    if(argc<1) {
        printf("Usage: %s graphmlfile", argv[0]);
    }

    graphmlfile=fopen(argv[1], "r");
    if(graphmlfile==NULL) {
        perror("Error: ");
        return(-1);
    }
    if(igraph_read_graph_graphml(&g, graphmlfile, 0)) {
        fprintf(stderr,"Error parsing graph");
        return(-1);
    }

    fclose(graphmlfile);

    vertexcount=igraph_vcount(&g);
    
    
    edgecount=igraph_ecount(&g);

    maxdegree=0;    

    for(i=0;i<vertexcount;i++) {
        igraph_vector_init(&adjv, 1);
        igraph_neighbors(&g,&adjv , i, IGRAPH_ALL);
        size=igraph_vector_size(&adjv);
        if(size>maxdegree) maxdegree=size;
        igraph_vector_destroy(&adjv);
    }

    printf("%d\n",maxdegree);
}
