#include <stdio.h>
#include <glib.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <unistd.h>

int completed_msg; 
long unsigned int tot_latency;
char log_file[1024];
long unsigned int tot_unique_msg_recv, tot_delay,tot_msgids;
long unsigned int tot_couples_msg_recv;
double tot_fraction_infected;

GHashTable* hash_msgrecv;
GHashTable* hash_countmsg;
GHashTable* hash_latencycount;

/*inline uint64_t hash_two_ints(uint32_t a, uint32_t b)
{
    return (a > b) ? (((uint64_t) a << 32) | b) : (((uint64_t) b << 32) | a);
}*/

inline uint64_t hash_two_ints(uint32_t msg, uint32_t node)
{
    /*uint64_t A = (ulong)(a >= 0 ? 2 * (uint64_t)a : -2 * (uint64_t)a - 1);
    uint64_t B = (ulong)(b >= 0 ? 2 * (uint64_t)b : -2 * (uint64_t)b - 1);
    uint64_t C = (long)((A >= B ? A * A + A + B : A + B * B) / 2);
    return a < 0 && b < 0 || a >= 0 && b >= 0 ? C : -C - 1;*/
    uint64_t a=msg;
    uint64_t b=node;
    return a >= b ? a * a + a + b : a + b * b;
}

void free_data (gpointer data) {
    free(data);
}

void get_pairs_stats(uint32_t *key, short unsigned int numbernodes, bool completed) {
    short unsigned int delay, latency;
    unsigned int node;
    unsigned int covered_nodes;
    uint64_t nodemsgkey;
    gpointer currlatency;

    delay=0;
    latency=0;
    covered_nodes=0;
    for(node=0; node<numbernodes; node++) {
        nodemsgkey=hash_two_ints(node, *key);
        currlatency = g_hash_table_lookup(hash_latencycount, &nodemsgkey);
        if(currlatency!=NULL) {
            covered_nodes++;
            tot_unique_msg_recv++;
            delay=*(short unsigned int*) currlatency;
            tot_delay+=delay;
            if (completed && (*(short unsigned int*) currlatency) > latency)
                latency=*(short unsigned int*) currlatency;
            g_hash_table_remove(hash_latencycount, &nodemsgkey);
        }
    }
    tot_latency+=latency;
}

gboolean analyze (uint32_t* key, uint32_t* value,  void *numbernodes) {
    if(*(int*) value == *(short unsigned int*) numbernodes) {
        completed_msg++;
        get_pairs_stats( key, *(short unsigned int*) numbernodes, true);
    }
    else {
        get_pairs_stats(key, *(short unsigned int*)numbernodes, false);
    }
    tot_msgids++;
    return TRUE;
}


void parse_log_file(char * file_name) {
    int errcode,unused,count;
    uint32_t recvid, msgid;
    short int delta;
    short int ttl;
    FILE* msgfile;
    gpointer msgval,msgkey;
    gpointer tmp;
    uint64_t *couplemsglatency;
    long currbyte,filesize;
    double filepercentage,oldpercentage;
    gpointer nhops;

    msgfile=fopen(file_name, "r");

    if(msgfile==NULL) {
        perror("Error opening trace: ");
        exit(-1);
    }
    
    fseek(msgfile, 0L, SEEK_END);
    filesize = ftell(msgfile);


    fseek(msgfile, 0L, SEEK_SET);

    tot_latency=0;
    tot_msgids=0;
    tot_couples_msg_recv=0;
    oldpercentage=0;
    do {

        do {
            errcode=fscanf(msgfile, "G %d\n", &unused);
        } while(errcode>0);

        errcode=fscanf(msgfile, "R %" SCNu32 " %" SCNu32 " %hd %hd\n",&recvid,  &msgid, &delta, &ttl);

        if(errcode == 4) tot_couples_msg_recv++;

        if(errcode==EOF) break;

        //sprintf(buf, "%d.%d", recvid,msgid);
        
        //msgid++;
        
        msgkey=malloc(sizeof(uint32_t));
        nhops=malloc(sizeof(short int));
        msgval=malloc(sizeof(int));

        if(msgkey==NULL || nhops==NULL || msgval==NULL) {
            fprintf(stderr, "Memory allocation failed, terminating...");
            exit(-1);
        }

        *(unsigned int*)msgkey=msgid;
        *(short int*)nhops=delta;

        //couplemsglatency=malloc(strlen(buf)+1);
        //strcpy((char*)couplemsglatency, buf);
        couplemsglatency=malloc(sizeof(uint64_t));


        if(couplemsglatency==NULL) {
            fprintf(stderr, "Memory allocation failed, terminating...");
            exit(-1);
        }

        *couplemsglatency=hash_two_ints(recvid, msgid);

        if((tmp=g_hash_table_lookup(hash_latencycount, couplemsglatency))!=NULL ) {
            if( *(short int*) nhops < *(short int*) tmp) {
                g_hash_table_replace(hash_latencycount, couplemsglatency,nhops);
            }
            else{
                free(couplemsglatency);
                free(nhops);
            }
                free(msgval);
                free(msgkey);
        }
        else {
            if((tmp=g_hash_table_lookup(hash_countmsg, msgkey))!=NULL) {
                count= (*(int*)tmp)+1;
            }
            else count=1;

            *(int*)msgval=count;

            g_hash_table_replace(hash_countmsg, msgkey, msgval);

            g_hash_table_insert(hash_latencycount, couplemsglatency, nhops);
        }
        currbyte=ftell(msgfile);
        filepercentage=((double)currbyte/filesize)*100.0;
        if((filepercentage - oldpercentage)>=1) {
        fprintf(stderr, "PROGRESS: %f%%\n", filepercentage);
        oldpercentage=filepercentage;
        }
        

    } while (errcode > 2);
}


int main (int argc, char **argv) {
    int current_lp,num_lps;
    int numbernodes;
    double reliability;

    if(argc < 3) {
        fprintf(stderr, "%s: tracedir numbernodes lps\n", argv[0]);
        exit(-1);
    }

    hash_countmsg =  g_hash_table_new_full(g_int_hash, g_int_equal,  (GDestroyNotify)free_data, (GDestroyNotify)free_data);
    hash_latencycount = g_hash_table_new_full(g_int64_hash, g_int64_equal,  (GDestroyNotify)free_data, (GDestroyNotify)free_data);

    numbernodes=atoi(argv[2]);
    num_lps = atoi(argv[3]);


    // Write log file
    for(current_lp=0; current_lp<num_lps; current_lp++) {
        sprintf(log_file, "%s/SIM_TRACE_%03d.log", argv[1], current_lp);
        fprintf(stderr, "Parsing file %s\n", log_file);
        parse_log_file(log_file);
    }

    fprintf(stderr, "Finished parsing file(s)\n");

    completed_msg=0;
    tot_delay=0;
    tot_unique_msg_recv=0;
    tot_fraction_infected=0;


    fprintf(stderr, "Begin foreach\n");
    g_hash_table_foreach_remove(hash_countmsg, analyze, &numbernodes);
    
    //printf("tot msgids %d\n", tot_msgids);

    reliability = ((double) completed_msg / (double) tot_msgids) * 100;

    //printf("#Nodes\t%%Coverage\tDelay\t#Msg\tOverhead\tReliability\tLatency\n");
    // Nodes
    printf("%d\t",numbernodes);
    // Coverage
    printf("%f\t", ((double) tot_unique_msg_recv/((double)tot_msgids*numbernodes))*100.0);
    // Delay
    printf("%f\t", (double) tot_delay/tot_unique_msg_recv);
    // Messages
    printf("%lu\t", tot_couples_msg_recv);
    // Overhead
    printf("%f\t",  (double) tot_couples_msg_recv/ ((double)numbernodes*tot_msgids));
    // Reliability
    printf("%f\t", reliability);
    // Latency
    if(completed_msg!=0)
        printf("%f\t", (double) tot_latency/(double) completed_msg);
    else
        printf("%d\t", completed_msg);
    return 0;
}
