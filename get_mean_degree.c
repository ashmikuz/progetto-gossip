#include<stdio.h>
#include<igraph.h>
#include<math.h>

int main(int argc, char **argv) {
    int vertexcount, edgecount;
    double result;
    FILE* graphmlfile;
    igraph_t g;

    if(argc<1) exit(-1);

    graphmlfile=fopen(argv[1], "r");
    if(graphmlfile==NULL) {
        perror("Error: ");
        return(-1);
    }
    if(igraph_read_graph_graphml(&g, graphmlfile, 0)) {
        fprintf(stderr,"Error parsing graph");
        return(-1);
    }

    fclose(graphmlfile);
    
    vertexcount=igraph_vcount(&g);
    edgecount=igraph_ecount(&g);
    result=((2.0*edgecount)/vertexcount);

    printf("%f\n", result);

}
